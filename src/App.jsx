/* eslint no-eval: 0*/
import React, { useState } from "react";
import words from 'lodash.words';
import Functions from "./components/Functions/Functions"
import Math from "./components/Math/Math";
import Number from "./components/Numbers/Number";
import Result from "./components/Result/Result";
import "./App.css";


const App = () => {
  const [stackResult, setResult] = useState("");
  const items = words(stackResult , /[^-^+^*^/]+/g);
  const value = items.length > 0 ? items[items.length-1] : 0 ;

  const clickHandlerFunction = (text) =>{
    setResult( `${stackResult}${text}`)
  }
  const onClickOperationFuntion = ( operation ) =>{
    setResult(`${stackResult}${operation}`)
  }
  const onClickEqualFuntion = ( result ) =>{
    setResult(eval(stackResult).toString())
  }
  return (
    <main className="react-calculator">
      <Result value={value}/>
      <Number onClickNumber={clickHandlerFunction} />
      <Math onClickOperation={onClickOperationFuntion} 
      onClickEqual={onClickEqualFuntion} />
      <Functions 
        onContentClear={() => setResult("")}
        onDelete={() => {
          if(stackResult.length > 0){
            const newStack = stackResult.substring(0, stackResult.length -1);
            setResult(newStack);
          }
        }}
      />
    </main>
  );
};

export default App;
