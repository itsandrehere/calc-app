import React from 'react';
import PropTypes from 'prop-types';

import './Result.css';

const Result = (props) => {
    console.log(props);
    const { value } = props;
    return (
      <div className="result">
        { value }
      </div>
    );
}

Result.propTypes = {
  value : PropTypes.string.isRequired
}

Result.defaultProps = {
  value: "0"
}
export default Result;