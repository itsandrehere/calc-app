import React from 'react';
import PropTypes from 'prop-types';
import Button from '../Button';

const Functions = ({ onDelete, onContentClear }) => {
  return (
    <section className="funtions">
      <Button text="Clear" type="button-long-text" clickHandler={onContentClear} />
      <Button text="&larr;" type="" clickHandler={onDelete}/>
    </section>
  )
}

Functions.propTypes = {
  onDelete: PropTypes.func.isRequired,
  onContentClear: PropTypes.func.isRequired
}

export default Functions;