import React from 'react';
import PropTypes from 'prop-types';
import Button from '../Button';

const Math = ({ onClickOperation, onClickEqual }) => {
  return (
    <section className="mathe">
      <Button text="+" clickHandler={onClickOperation} />
      <Button text="-" clickHandler={onClickOperation} />
      <Button text="*" clickHandler={onClickOperation} />
      <Button text="/" clickHandler={onClickOperation} />
      <Button text="=" clickHandler={onClickEqual} />
    </section>
  )
};

Math.protoTypes = {
  onClickOperation: PropTypes.func.isRequired,
  onClickEqual: PropTypes.func.isRequired
}

export default Math;